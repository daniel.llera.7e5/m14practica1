import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit {

  intentosrestantes: number = 0;
  intentos = ""
  dificultat = ""
  palabra = ""
  palabraauxiliar = " ";
  url = " "
  win = "gfh"
  img = ""
  numerodeimagen = 0
  arrayPalabra = []

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService) {

    this.route.queryParams.subscribe(params => {
      if (params && params.dificultat) {
        this.dificultat = params.dificultat;
        
      }
    });
  }

  ionViewWillEnter() {
    this.ngOnInit();
    this.reoladGame();
  }

  ngOnInit() {
    this.palabra = this.dataService.getPalabra(this.dificultat)
    let vocales = [];
    let todas = [];
    this.url = "../assets/img/imagen1.png"
    this.numerodeimagen = 1
    this.intentosrestantes = 6
    this.palabraauxiliar = this.palabra
    this.intentos = "Intentos Restantes:" + this.intentosrestantes

    function esVocal(caracter) {
      return caracter == "a" || caracter == "e" || caracter == "i" || caracter == "o" || caracter == "u";
    }
    function estodas(caracter) {
      return caracter == "a" || caracter == "e" || caracter == "i" || caracter == "o" || caracter == "u" || caracter == "b" || caracter == "c" || caracter == "d" || caracter == "f" || caracter == "g" || caracter == "h" || caracter == "j" || caracter == "k" || caracter == "l" || caracter == "m" || caracter == "n" || caracter == "p" || caracter == "q" || caracter == "r" || caracter == "s" || caracter == "t" || caracter == "w" || caracter == "x" || caracter == "y" || caracter == "z" || caracter == "v";
    }
    if (this.dificultat == "facil") {
      for (var i = 0; i < this.palabra.length; i++) {
        var caracter = this.palabra.charAt(i);
        if (esVocal(caracter)) {
          vocales.push(caracter);
          this.palabraauxiliar = this.palabraauxiliar.replace(caracter, ' _ ');

        }
      }
    } else {
      for (var i = 0; i < this.palabra.length; i++) {
        var caracter = this.palabra.charAt(i);
        if (estodas(caracter)) {
          todas.push(caracter);
          this.palabraauxiliar = this.palabraauxiliar.replace(caracter, '_ ');

        }
      }

    }
    console.log("Vocales:  ", todas);

    console.log(this.palabra)

    this.arrayPalabra = this.palabraauxiliar.split(" ")
  }

  onClick(letra, event) {

    for (let i = 0; i < this.palabra.length; i++) {
      if (letra == this.palabra.charAt(i)) {
        this.arrayPalabra[i] = this.arrayPalabra[i].replace("_", letra)
        this.palabraauxiliar = this.arrayPalabra.join(" ")

        console.log("array" + this.arrayPalabra)
      }

      if (!this.palabraauxiliar.includes("_")) {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            intentos: this.intentos,
            dificultat: this.dificultat,
            win: "As ganado",
            img: "../assets/img/win.png"
            
          }
        };
        this.router.navigate(['resultados'], navigationExtras);
      }
    }

    if (!this.palabra.includes(letra)) {
      this.numerodeimagen = this.numerodeimagen + 1
      this.url = "../assets/img/imagen" + this.numerodeimagen + ".png"
      this.intentosrestantes -= 1
      this.intentos = "Intentos Restantes: " + this.intentosrestantes

      if (this.intentosrestantes == 0) {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            intentos: this.intentos,
            dificultat: this.dificultat,
            win:  "As perdido",
            img: "../assets/img/imagen7.png"
          }
        };
        this.router.navigate(['resultados'], navigationExtras);
      }
    }

    console.log(this.palabraauxiliar)
    console.log(this.palabra)
    console.log(this.intentos);

    event.srcElement.setAttribute("disabled", true)
  }

  reoladGame(){
     document.getElementById('a').setAttribute("disabled", 'false');
     document.getElementById('b').setAttribute("disabled", 'false');
     document.getElementById('c').setAttribute("disabled", 'false');
     document.getElementById('d').setAttribute("disabled", 'false');
     document.getElementById('e').setAttribute("disabled", 'false');
     document.getElementById('f').setAttribute("disabled", 'false');
     document.getElementById('g').setAttribute("disabled", 'false');
     document.getElementById('h').setAttribute("disabled", 'false');
     document.getElementById('i').setAttribute("disabled", 'false');
     document.getElementById('j').setAttribute("disabled", 'false');
     document.getElementById('k').setAttribute("disabled", 'false');
     document.getElementById('l').setAttribute("disabled", 'false');
     document.getElementById('m').setAttribute("disabled", 'false');
     document.getElementById('n').setAttribute("disabled", 'false');
     document.getElementById('ñ').setAttribute("disabled", 'false');
     document.getElementById('o').setAttribute("disabled", 'false');
     document.getElementById('p').setAttribute("disabled", 'false');
     document.getElementById('q').setAttribute("disabled", 'false');
     document.getElementById('r').setAttribute("disabled", 'false');
     document.getElementById('s').setAttribute("disabled", 'false');
     document.getElementById('t').setAttribute("disabled", 'false');
     document.getElementById('u').setAttribute("disabled", 'false');
     document.getElementById('v').setAttribute("disabled", 'false');
     document.getElementById('w').setAttribute("disabled", 'false');
     document.getElementById('x').setAttribute("disabled", 'false');
     document.getElementById('y').setAttribute("disabled", 'false');
     document.getElementById('z').setAttribute("disabled", 'false');

 }
  

}
