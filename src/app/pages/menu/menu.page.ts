import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ToastController, ToastOptions } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  toastOptions: ToastOptions
  dificultat: string = '';
  toast;
  constructor(private toastcontroller: ToastController, private router: Router) {

  }

  ngOnInit() {
  }

  showtoast() {
    this.toast = this.toast = this.toastcontroller.create({
      message: 'En este ahorcado existen 2 modos: \nModo Facil: Solo se ocultan las vocales \nModo Dificil: Se oculta toda la palabra',
      duration: 4000
    }).then((toastData) => {
      console.log(toastData)
      toastData.present()
    })
  }
  game() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultat: this.dificultat
      }
    };
    this.router.navigate(['game'], navigationExtras)
  }

}
