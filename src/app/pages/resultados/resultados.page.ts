import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.page.html',
  styleUrls: ['./resultados.page.scss'],
})
export class ResultadosPage implements OnInit {
  dificultat = ""
  intentos = ""
  win = ""
  img = ""
  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService) {

    this.route.queryParams.subscribe(params => {
      if (params && params.dificultat) {
        this.dificultat = params.dificultat;
        this.intentos = params.intentos;
        this.win = params.win;
        this.img = params.img;
      }
    });
  }

  ngOnInit() {
  }

  playagain() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        intentos: this.dificultat
      }
    };
    this.router.navigate(['game'], navigationExtras);
  }

}
