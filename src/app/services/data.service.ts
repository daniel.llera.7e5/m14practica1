import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  facil = ["pavo", "navidad", "regalos", "cometa", "vela", "pajes"]
  dificil = ["pandereta", "peladillas", "papanoel", "polvoron", "turron", "villancicos", "estrellas"]
  constructor() { }

  getPalabra(dificultat) {
    if (dificultat == 'facil') {
      let palabrafacil = this.facil[Math.floor(Math.random() * this.facil.length)];
      return palabrafacil;
    } else {
      let palabradificil = this.dificil[Math.floor(Math.random() * this.dificil.length)];
      return palabradificil;
    }

  }
}

